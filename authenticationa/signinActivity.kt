package com.example.authenticationa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.activity_signin.emailEditText
import kotlinx.android.synthetic.main.activity_signin.passwordEditText
import kotlinx.android.synthetic.main.activity_signup.*

class signinActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signInButton.setOnClickListener{
            signIn()

        }


    }
    private fun signIn(){
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()){
            progresBar1.visibility = View.VISIBLE
            signUpButton.isClickable = false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progresBar1.visibility = View.GONE
                    signInButton.isClickable = true
                    if (task.isSuccessful) {
                        d("signIn", "signInWithEmail:success")
                        Toast.makeText(this, "Authentication is Success!", Toast.LENGTH_SHORT).show()
                        val user = auth.currentUser
                    } else {
                        d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            this, task.exception.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
        }else {
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show()
        }
    }



}