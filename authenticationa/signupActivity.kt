package com.example.authenticationa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlin.math.sign

class signupActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()

        }

    }

    private fun signUp() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                progresBar.visibility = View.VISIBLE
                signUpButton.isClickable = false
                if (email.matches(emailPattern.toRegex())) {
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            progresBar.visibility = View.GONE
                            signUpButton.isClickable = true
                            if (task.isSuccessful) {
                                d("signUp", "createUserWithEmail:success")
                                Toast.makeText(this, "SignUp is success!", Toast.LENGTH_SHORT)
                                    .show()
                                val user = auth.currentUser

                            } else {
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(
                                    this, task.exception.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                        }
                }else {
                    Toast.makeText(applicationContext, "Email format is not Correct",
                        Toast.LENGTH_SHORT).show()
                }



            } else {
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "Please, fill all fields", Toast.LENGTH_SHORT).show()
        }

    }






}