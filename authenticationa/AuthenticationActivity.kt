package com.example.authenticationa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        signUp.setOnClickListener{
            val intent = Intent(this,signupActivity::class.java)
            startActivity(intent)


        }
        signIn.setOnClickListener{
            val intent = Intent(this,signinActivity::class.java)
            startActivity(intent)

        }
    }
}